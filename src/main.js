// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Cesium from 'cesium/Cesium'
import '../node_modules/cesium/Build/Cesium/Widgets/widgets.css'
// import VideoPlayer from 'vue-video-player'
// require('video.js/dist/video-js.css')
// require('vue-video-player/src/custom-theme.css')
import App from './App'
import './assets/fonts/iconfont.css';
import router from './router'
import axios from 'axios'
import data from '../static/serverconfig.json';
// import 'videojs-flash'
// import 'videojs-contrib-hls'



Vue.config.productionTip = false
Vue.prototype.axios = axios
Vue.prototype.Cesium = Cesium
Vue.use(ElementUI)
// Vue.use(VideoPlayer)
//在main.js中定义一个全局变量，加载并获取serverconfig.json文件里面的baseUrl
Vue.prototype.GLOBAL=data;

axios.defaults.baseURL = data.baseUrl;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
